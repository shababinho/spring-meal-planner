package net.therap.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author shabab
 * @since 3/14/18
 */
@Entity
@Table(name = "meal_items")
public class MealItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private long id;

    @ManyToOne
    @JoinColumn(name = "meal_id")
    private Meal meal;

    public MealItem() {
    }

    public MealItem(Meal meal, String description) {
        this.meal = meal;
        this.description = description;
    }

    private String description;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Meal getMeal() {
        return meal;
    }

    public void setMeal(Meal meal) {
        this.meal = meal;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return String.format("%d\t%s\t%s\t%s", getId(), getMeal().getMealDay(),
                getMeal().getMealTime(), getDescription());
    }
}
