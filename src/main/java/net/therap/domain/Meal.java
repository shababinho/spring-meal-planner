package net.therap.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shabab
 * @since 3/14/18
 */
@Entity
@Table(name = "meals")
public class Meal implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private long id;

    @Column(name = "meal_day")
    private String mealDay;

    @Column(name = "meal_time")
    private String mealTime;

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            mappedBy = "meal"
    )
    private List<MealItem> mealItems;

    public Meal(String mealDay, String mealTime) {
        this.mealDay = mealDay;
        this.mealTime = mealTime;
        this.mealItems = new ArrayList<MealItem>();
    }

    public Meal() {
        this.mealItems = new ArrayList<MealItem>();
    }

    public void addMealItem(MealItem mealItem) {
        mealItems.add(mealItem);
        mealItem.setMeal(this);
    }

    public void removeComment(MealItem mealItem) {
        mealItems.remove(mealItem);
        mealItem.setMeal(null);
    }

    public List<MealItem> getMealItems() {
        return mealItems;
    }

    public void setMealItems(List<MealItem> mealItems) {
        this.mealItems = mealItems;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMealDay() {
        return mealDay;
    }

    public void setMealDay(String mealDay) {
        this.mealDay = mealDay;
    }

    public String getMealTime() {
        return mealTime;
    }

    public void setMealTime(String mealTime) {
        this.mealTime = mealTime;
    }

    @Override
    public String toString() {
        return String.format("%s\t---\t%s", getMealTime(), getMealDay());
    }
}
