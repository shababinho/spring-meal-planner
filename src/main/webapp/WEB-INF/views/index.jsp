<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <jsp:include page="include/header.jsp">
        <jsp:param name="title" value="Meal Planner| Home"/>
    </jsp:include>
</head>

<body>
<div class="content">
    <jsp:include page="include/navbar.jsp"/>

    <div class="container-fluid">
        <c:if test="${not empty requestScope.success}">
            <c:choose>
                <c:when test="${requestScope.success == true}">
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Success!!</strong> ${requestScope.message}
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Oooops!!</strong> ${requestScope.message}
                    </div>
                </c:otherwise>
            </c:choose>
        </c:if>

        <h2>Home</h2>

        <p>
            Here is the list of all meals:
        </p>

        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Day</th>
                    <th>Time</th>
                    <th>Meal Name</th>
                </tr>
                </thead>

                <tbody>
                <c:forEach items="${requestScope.mealItems}" var="mealItem" varStatus="loop">
                    <tr>
                        <td>${loop.index + 1}</td>
                        <td>${mealItem.meal.mealDay}</td>
                        <td>${mealItem.meal.mealTime}</td>
                        <td>${mealItem.description}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>


    </div>
</div>

<div id="footer">
    <jsp:include page="include/footer.jsp"/>
</div>
</body>

</html>