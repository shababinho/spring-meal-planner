<title>${param.title}</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Cookie|Kite+One|Pacifico|Roboto" rel="stylesheet"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"/>
<link href="${pageContext.request.contextPath.concat("/resources/styles/footer.css")}" rel="stylesheet" type="text/css"/>
<link href="${pageContext.request.contextPath.concat("/resources/styles/nav.css")}" rel="stylesheet" type="text/css"/>
<link href="${pageContext.request.contextPath.concat("/resources/styles/main.css")}" rel="stylesheet" type="text/css"/>
