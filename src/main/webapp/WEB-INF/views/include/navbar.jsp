<nav class="navbar  navbar-inverse navbar-static-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><h3><span class="first">Meal</span><span>Planner</span></h3></a>
        </div>

        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="${pageContext.request.contextPath}/home"><span class="nav-text">All Meals</span></a></li>
                <li><a href="${pageContext.request.contextPath}/add-meal"><span class="nav-text">Add Meal</span></a>
                </li>
                <li><a href="${pageContext.request.contextPath}/edit-meal"><span class="nav-text">Edit Meal</span></a>
                </li>
                <li><a href="${pageContext.request.contextPath}/delete-meal"><span
                        class="nav-text">Delete Meal</span></a></li>
            </ul>
        </div>
    </div>
</nav>

