<footer class="footer-distributed footer">

    <div class="footer-left">
        <h3>Meal<span>Planner</span></h3>

        <p class="footer-company-name">Meal Planner &copy; 2018</p>
    </div>

    <div class="footer-center">

        <div>
            <i class="fa fa-map-marker"></i>

            <p><span>Block C, 47 Rd No. 4</span> Dhaka 1213, Bangladesh</p>
        </div>

        <div>
            <i class="fa fa-phone"></i>

            <p>+880 1670737626</p>
        </div>

        <div>
            <i class="fa fa-envelope"></i>

            <p><a href="mailto:support@company.com">support@mealplanner.com</a></p>
        </div>

    </div>

    <div class="footer-right">

        <p class="footer-company-about">
            <span>About the company</span>
            Planning one meal at a time
        </p>

        <div class="footer-icons">

            <a href="#"><i class="fa fa-facebook"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-linkedin"></i></a>
            <a href="#"><i class="fa fa-instagram"></i></a>


        </div>

    </div>

</footer>